# no python, toda frase precisa ficar entre aspas.
 
print("meu nome é Edvaldo")
 
# Input é usado para coletar informações do usuário.
# input("qual é o teu nome?")
 
# Variáveis

# print("Confirmação de cadastro:")
# nome = input("Qual é o teu nome? ")
# cpf = input("Qual é o teu CPF? ")
# idade = input("Qual tua idade? ")

# print("Nome: " + nome)
# print("CPF: " + cpf)
# print("Idade: " + idade)

# variavel_str = "Soma:"
# variavel_int = 29
# variavel_int2 = 18
 
# print("Soma: ", variavel_int + variavel_int2)
# print("Diferença: ", variavel_int - variavel_int2)


# 1) Em muitos programas, nos é solicitado o preenchimento de algumas informações como
# nome, CPF, idade e unidade federativa. Escreva um script em Python que solicite as
# informações cadastrais mencionadas e que em seguida as apresente da seguinte forma:

# -----------------------------
# Confirmação de cadastro:
# Nome: Guido Van Rossum
# CPF: 999.888.777/66
# Idade: 65
# -----------------------------

nome = input("""Qual é o seu nome? """)
cpf = input("Qual é o seu CPF? ")
idade = input("Qual é a sua idade? ")

print(f"""
-----------------------------
Confirmação de cadastro:
Nome: {nome}
CPF: {cpf}
Idade: {idade}

-----------------------------
""")

# 2) Escreva um script em Python que receba dois números e que seja realizado as seguintes
# operações:
# • soma dos dois números
# • diferença dos dois números
# O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
# 4 e 2:

# ------------------------------
# Soma: 4 + 2 = 6
# Diferença: 4 - 2 = 2

num1 = int(input("Digite o primeiro numero: "))
num2 = int(input("Digite o segundo numero: "))

soma = num1 + num2
diferenca = num1 - num2

print(f"""
------------------------------
Soma: {soma}
Diferença: {diferenca}
""")

# IF - Condição

idade = int(input("Qual é a sua idade? "))

if idade ==+ 18:
    print("Voce é maior de idade.")

elif idade > 15:
    print("Voce tem pelo menos 15 anos.")

elif idade > 10:
    print("Voce tem pelo menos 10 anos.")

else:
    print("Voce tem menos de 10 anos.")


# FOR / WHILE - Repetição
    
for numero in range(0, 10):
    print(numero)


resposta = "nao"
while resposta != "sim":
    print("Imprimindo mensagem do loop")
    resposta = input("Quer parar o loop? [sim/nao] ")


while True:
    resposta = input("Quer parar o loop? [sim/nao] ")
    if resposta == "sim":
        break


# TUPLA / LISTA / DICIONARIO / SET - Coleções
    
tupla = ("Beterraba", "Mandioca", "Batata", 50, True)
# INDEX       0            1          2      3    4     

for cachorro in tupla:
    print(f"O objeto {cachorro} é um {type(cachorro)}")


lista = ["Beterraba", "Mandioca", "Batata", 50, True]
# INDEX       0            1          2      3    4

lista.append("Pão de batata")
lista.remove(50)
lista.pop(0)
lista.insert(2, "Avião")

print(lista)


# Métodos em Strings
frase = "Testando uma nova frase"
print(frase.replace("a", "---"))
